(function(){

    var intercepter = function($q){
        return {
            request: function(config){
                config.headers = {
                    'Authorization' : 'Token token=' + localStorage.getItem('token'),
                    'Content-Type' : 'application/json'
                };
                return config;
            },
            response: function(result){
                return result;
            },

            responseError: function(rejection) {
                console.log('Failed with',rejection.status, 'status');
                if (rejection.status == 403) {
                    window.location = 'views/404.html';
                }
                return $q.reject(rejection);

            }
        }
    };


    App.service('intercepter',intercepter);
}());