var App = angular.module('appointmentSystem',['ui.router','ui.calendar','ui.bootstrap','flash']);

App.config(function($stateProvider, $urlRouterProvider, $httpProvider){

    $urlRouterProvider.otherwise('/companies/4/appointments');

    $stateProvider
        .state('company',{
            url: '/companies/:id',
            templateUrl: 'views/company.html',
            resolve: {
                company:function(crud_service, $stateParams){
                    console.log("StateParams",$stateParams);
                    return crud_service.get_me('companies/'+$stateParams.id);
                },
                companyId: ['$stateParams', function($stateParams){
                    return $stateParams.id;
                }]
            },
            controller: function ($scope, company) {
                console.log("app js controller",company);
                $scope.company = company.data;
            }
        })
        .state('company.appointments', {
            url: '/appointments',
            templateUrl: 'views/calendar.html',
            controller: 'CalendarCtrl'
        })
        .state('company.about',{
            url: '/about',
            templateUrl: 'views/about.html',
            controller: function(){}
        })
        .state('company.anotherUsers',{
            url: '/another_users',
            templateUrl: 'views/anotherUser.html',
            resolve: {
                users: function(crud_service){
                    return crud_service.get_me('companies');
                }
            },
            controller: function ($scope, users) {
                $scope.users = users.data;
            }
        })
        .state('404',{
            url: '/404',
            templateUrl :'views/404.html',
            controller: function(){}
        });


    $httpProvider.interceptors.push('intercepter');
});

