(function(){

    var newEventModalCtrl = function($scope, start, $modalInstance, crud_service, appointment_types) {

        $scope.types = appointment_types.data;
        $scope.selectedType = $scope.types[0];

    $scope.myAdd =  function(){
        var ldata = {"appointment": {
            appointment_type_id: $scope.selectedType.id,
            start: start,
            end: "",
            client: $scope.user.name,
            email: $scope.user.email,
            phone: $scope.user.phone
        }};

        var promise = crud_service.post_me('appointments',ldata);
        promise.then(function(response){
                $modalInstance.close(response);
                $modalInstance.dismiss();
        }, function(error){
            $modalInstance.close(error);
            $modalInstance.dismiss();
        });
    };

    };

    App.controller('newEventModalCtrl',newEventModalCtrl);
}());